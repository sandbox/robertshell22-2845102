<?php

namespace Drupal\evernote_sync\Controller;

use Guzzle\Plugin\Oauth\OauthPlugin;
use EDAM\NoteStore\NoteStoreClient;
use EDAM\NoteStore\NoteFilter;
use EDAM\NoteStore\NotesMetadataResultSpec;
use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\THttpClient;
use Evernote\Client;


class EvernoteSyncController {


  protected $baseUrl;
  protected $sandbox;
  protected $consumerKey;
  protected $consumerSecret;
  protected $callbackUrl;
  protected $requestTokenUrl = 'oauth';
  protected $accessTokenUrl = 'oauth';
  protected $authorizationUrl = 'OAuth.action?oauth_token=%s';

  protected $tokens;

  protected $oauthPlugin;
  protected $authToken;
  protected $noteStoreUrl;

  protected $protocol;

  protected $client;

  public function __construct($baseUrl, $authToken, $noteStoreUrl) {
    $this->baseUrl = 'https://sandbox.evernote.com/shard/s1/';
    $this->authToken = 'S=s1:U=934c8:E=1611dc0d74b:C=159c60fa840:P=1cd:A=en-devtoken:V=2:H=9997615f3fdf9d041931edb5dae69b16';
    $this->noteStoreUrl = 'https://sandbox.evernote.com/shard/s1/notestore';
  }


  public function requestTempCredentials() {
    return $this->requestCredentials(
      $this->requestTokenUrl . '?oauth_callback=' . $this->callbackUrl
    );
  }

  public function requestAuthCredentials($token, $tokenSecret, $verifier) {
    return $this->requestCredentials(
      $this->accessTokenUrl . '?oauth_callback=' . $this->callbackUrl . '&oauth_verifier=' . $verifier,
      $token,
      $tokenSecret
    );
  }

  protected function requestCredentials($url, $token = FALSE, $tokenSecret = FALSE) {
    $client = $this->getClient($token, $tokenSecret);

    $response = $client->post($url)->send();

    return $this->makeTokens($response);
  }

  protected function makeTokens($response) {
    $body = (string) $response->getBody();

    $tokens = array();
    parse_str($body, $tokens);

    if (empty($tokens)) {
      throw new \Exception("An error occurred while requesting oauth token credentials");
    }

    $this->tokens = 'S=s1:U=934c8:E=1611dc0d74b:C=159c60fa840:P=1cd:A=en-devtoken:V=2:H=9997615f3fdf9d041931edb5dae69b16';
    return $this->tokens;
  }

  public function getClient($token = FALSE, $tokenSecret = FALSE) {
    if (!is_null($this->client)) {
      return $this->client;
    }
    else {
      $this->client = new Client($this->baseUrl);

      $this->oauthPlugin = new OauthPlugin(array(
        'consumer_key' => $this->consumerKey,
        'consumer_secret' => $this->consumerSecret,
        'token' => !$token ? $this->tokens['oauth_token'] : $token,
        'token_secret' => !$token ? $this->tokens['oauth_token_secret'] : $tokenSecret,
      ));

      $this->client->addSubscriber($this->oauthPlugin);

      return $this->client;
    }
  }

  public function makeAuthUrl() {
    return $this->baseUrl . sprintf($this->authorizationUrl, urlencode($this->tokens['oauth_token']));
  }

  public function setConsumerKey($consumerKey) {
    $this->consumerKey = $consumerKey;
    return $this;
  }

  public function setConsumerSecret($consumerSecret) {
    $this->consumerSecret = $consumerSecret;
    return $this;
  }

  public function setCallbackUrl($callbackUrl) {
    $this->callbackUrl = $callbackUrl;
    return $this;
  }

  public function setRequestTokenUrl($requestTokenUrl) {
    $this->requestTokenUrl = $requestTokenUrl;
    return $this;
  }

  public function setAccessTokenUrl($accessTokenUrl) {
    $this->accessTokenUrl = $accessTokenUrl;
    return $this;
  }

  public function setAuthorizationUrl($authorizationUrl) {
    $this->authorizationUrl = $authorizationUrl;
    return $this;
  }

  public function setPrivateKey($privateKey) {
    $this->privateKey = $privateKey;
    return $this;
  }


  public function loadProtocol() {
    if (is_null($this->protocol)) {
      $parts = parse_url($this->noteStoreUrl);
      $parts['port'] = ($parts['scheme'] === 'https') ? 443 : 80;

      $client = new THttpClient($parts['host'], $parts['port'], $parts['path'], $parts['scheme']);

      $this->protocol = new TBinaryProtocol($client);
    }
    return $this->protocol;
  }

  public function loadClient() {
    if (is_null($this->client)) {
      $protocol = $this->loadProtocol();
      $this->client = new NoteStoreClient($protocol, $protocol);
    }
    return $this->client;
  }

  public function syncState() {
    $client = $this->loadClient();
    return $client->getSyncState($this->authToken);
  }

  public function listNotebooks() {
    $client = $this->loadClient();
    return $client->listNotebooks($this->authToken);
  }

  public function listNotes($notebookGuid) {
    $client = $this->loadClient();

    $counts = $client->findNoteCounts($this->authToken, new NoteFilter(), FALSE);
    $total = $counts->notebookCounts[$notebookGuid];

    $spec = new NotesMetadataResultSpec();
    $spec->includeTitle = TRUE;
    $spec->includeContentLength = TRUE;
    $spec->includeCreated = TRUE;
    $spec->includeUpdated = TRUE;
    $spec->includeUpdateSequenceNum = TRUE;
    $spec->includeNotebookGuid = TRUE;

    $notes = $client->findNotesMetadata($this->authToken, new NoteFilter(), 0, $total + 1, $spec);

    foreach ($notes->notes as $key => $note) {
      $notes->notes[$key]->created = round($note->created / 1000);
      $notes->notes[$key]->updated = round($note->updated / 1000);
    }

    return $notes->notes;

  }

  public function getNote($noteGuid) {
    $client = $this->loadClient();
    $note = $client->getNote($this->authToken, $noteGuid, TRUE, TRUE, TRUE, TRUE);

    $note->created = round($note->created / 1000);
    $note->updated = round($note->updated / 1000);
    $note->tags = array();

    if (isset($note->tagGuids)) {
      $allTags = $this->listTags();

      foreach ($note->tagGuids as $guid) {
        if (isset($allTags[$guid])) {
          $note->tags[] = $allTags[$guid];
        }
      }
    }

    return $note;
  }

  public function getNoteAsHtml($noteGuid) {
    $note = (object) $this->getNote($noteGuid);

    $doc = new \DOMDocument();
    $doc->loadXML($note->content);

    // Loop through every resource and embed it
    foreach ($doc->getElementsByTagName('en-media') as $media) {
      $type = $media->getAttribute('type');
      $hash = $media->getAttribute('hash');

      $imgSrc = FALSE;
      foreach ($note->resources as $resource) {
        // Is there a better way to match this hash?
        if (FALSE !== strpos($resource->recognition->body, $hash)) {
          $imgSrc = $resource->data->body;
        }
      }

      if ($imgSrc) {
        $img = $doc->createElement('img');
        $img->setAttribute('src', 'data:' . $type . ';base64,' . base64_encode($imgSrc));

        $media->parentNode->replaceChild($img, $media);
      }
    }

    $dom = new \DOMDocument();
    $dom->loadXml("<div></div>");

    foreach ($doc->getElementsByTagName('en-note')
               ->item(0)->childNodes as $child) {
      $node = $dom->importNode($child, TRUE);

      $dom->documentElement->appendChild($node);
    }

    $note->content = $dom->saveHTML($dom->documentElement);

    return $note;
  }

  public function listTags() {
    $client = $this->loadClient();
    $tags = $client->listTags($this->authToken);

    $tagsByGuid = array();

    foreach ($tags as $tag) {
      $tagsByGuid[$tag->guid] = $tag->name;
    }

    return $tagsByGuid;
  }

  public function listTheNotebooks() {
    $client = $this->loadClient();
    $notebooks = $client->listNotebooks('S=s1:U=934c8:E=1611dc0d74b:C=159c60fa840:P=1cd:A=en-devtoken:V=2:H=9997615f3fdf9d041931edb5dae69b16');

    $notebooksByList = array();

    foreach ($notebooks as $notebook) {

      echo "\n\nName : " . $notebook->name;

      echo "\nGuid : " . $notebook->guid;

      echo "\n\nCreated : " . $notebook->serviceCreated;

      echo "\nUpdated : " . $notebook->serviceUpdated;

      echo "\nStack : " . $notebook->stack;

    }
    return $notebooksByList;
  }

}