<?php

/**
 * @file
 * Contains Drupal\evernote_sync\Form\EvernoteSyncSettingsForm.
 */

namespace Drupal\evernote_sync\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Class EvernoteSyncSettingsForm.
 *
 * @package Drupal\evernote_sync\Form
 */
class EvernoteSyncSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'evernote_sync.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'evernote_sync_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = \Drupal::config('evernote_sync.settings');
    return array(
      'consumer_key' => $default_config->get('evernote_sync.consumer_key'),
      'secret_key' => $default_config->get('evernote_sync.secret_key')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('evernote_sync.settings');
    $form['text']['#markup'] = t('<br> This module allows you to authenticate to Evernote using OAUTH and sync notes between your Evernote account and Drupal. <br> Get an Evernote API key from: <a href="https://dev.evernote.com/doc/" target="_blank">https://dev.evernote.com/doc/</a>. On that page, click "<strong>GET AN API KEY</strong>" and make sure to select "<strong>Full Access</strong>" permissions.<br> ');
    $form['auth']['#markup'] = t('<br><hr><br><h4>Settings</h4>');
    $form['consumer_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Evernote API Consumer Key'),
      '#description' => t('The consumer key provided by Evernote to access the API.'),
      '#default_value' => $config->get('evernote_sync.consumer_key'),
    );

    $form['secret_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Evernote API Secret Key'),
      '#description' => t('The secret key provided by Evernote to access the API.<br>'),
      '#default_value' => $config->get('evernote_sync.secret_key'),
    );
    $options = array(
      'https://sandbox.evernote.com/' => 'Sandbox (Test)',
      'https://www.evernote.com/' => 'Production (International)',
      'https://app.yinxiang.com/' => 'Production (China)'
    );
    $form['evernote_server'] = array(
      '#type' => 'select',
      '#title' => $this->t('Evernote API Server'),
      '#default_value' => 'Sandbox (Test)',
      '#options' => $options,
      '#description' => t('<br>'),
    );
    $form['delete_auth'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('<strong>Delete Current Authorization</strong>'),
      '#default_value' => 0,
      '#description' => t('Delete the current Evernote authorization. This will force a reauthorization to import notes.'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save Settings',
    );

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('evernote_sync.settings')
      ->set('evernote_sync.consumer_key', $form_state->getValue('consumer_key'))
      ->set('evernote_sync.secret_key', $form_state->getValue('secret_key'))
      ->set('evernote_sync.evernote_server', $form_state->getValue('evernote_server'))
      ->set('evernote_sync.delete_auth', $form_state->getValue('delete_auth'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}